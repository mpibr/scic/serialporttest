int i = 0;
const size_t expectedBytes = sizeof(uint32_t);
void setup() {
  // Start the main serial communication
  Serial.begin(9600);
  
  // Start the secondary serial communication
  Serial1.begin(9600);
  Serial1.println("Arduino started");
}

void loop() {

  uint8_t buffer[expectedBytes];
  // Variable to store the assembled uint32_t value
  uint32_t receivedValue;

  // Check if the expected number of bytes has arrived
  if (Serial.available() >= expectedBytes) {
    // Read the incoming bytes into the buffer
    for (size_t i = 0; i < expectedBytes; ++i) {
      buffer[i] = Serial.read();
    }

    // Reassemble the bytes into a uint32_t value
    receivedValue = (uint32_t)buffer[0] |
                    (uint32_t)buffer[1] << 8 |
                    (uint32_t)buffer[2] << 16 |
                    (uint32_t)buffer[3] << 24;

    
    Serial1.println("Data available");
    Serial1.println(receivedValue);
  }
  Serial1.println(i);
  i++;
  delay(1000);
}
