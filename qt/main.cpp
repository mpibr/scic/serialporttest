#include <QSerialPort>
#include <QByteArray>
#include <QCoreApplication>
#include <QDebug>
#include <QThread>

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    QSerialPort serialPort;
    serialPort.setPortName("/dev/ttyACM0"); // Replace with your port name
    serialPort.setBaudRate(QSerialPort::Baud9600);
    serialPort.setDataBits(QSerialPort::Data8);
    serialPort.setParity(QSerialPort::NoParity);
    serialPort.setStopBits(QSerialPort::OneStop);
    serialPort.setFlowControl(QSerialPort::NoFlowControl);

    if (!serialPort.open(QIODevice::ReadWrite)) {
        qCritical() << "Failed to open port";
        return 1;
    }


    while(true){
        uint32_t value = 60000; // Replace with the value you want to send
        QByteArray byteArray;
        byteArray.append(reinterpret_cast<const char*>(&value), sizeof(value));

        qint64 bytesWritten = serialPort.write(byteArray);
        serialPort.flush();

        if (bytesWritten == -1) {
            qCritical() << "Failed to write the data to port";
            return 1;
        } else if (bytesWritten != byteArray.size()) {
            qCritical() << "Failed to write all the data to port";
            return 1;
        } else {
            qDebug() << "Data written to port successfully";
        }

        QThread::sleep(1);
    }


    serialPort.close();
    return 0;
}
